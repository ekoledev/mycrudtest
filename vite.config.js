import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
require('dotenv').config()

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@src': path.resolve(__dirname, './src'),
      '@assets': path.resolve(__dirname, './assets'),
    },
  },
  minify: 'terser',
  define: {
    'process.env': process.env
  },
  env: {
    API_URL_PRODUCTS : process.env.API_URL_PRODUCTS,
    API_URL_PRODUCT : process.env.API_URL_PRODUCT,
    API_URL_CATEGORIES: process.env.API_URL_CATEGORIES,
    API_URL_MEASURES: process.env.API_URL_MEASURES,
    ACCESS_TOKEN : process.env.ACCESS_TOKEN
  }
})
