import { createRouter, createWebHistory } from 'vue-router'


import Main from '/src/pages/Main.vue'
import Admin from '/src/pages/admin/Admin.vue'
import Products from '/src/pages/admin/Products.vue'
import Product from '/src/pages/admin/Product.vue'


const routes = [
    {
        path: '/',
        name: 'Main',
        component: Main,
    },
    {
        path: '/admin',
        name: 'Admin',
        component: Admin,
        children: [
            {
                path: 'products',
                name: 'Products',
                component: Products
            },
            {
                path: ':id/product',
                name: 'Product',
                component: Product,
                props: true
            }
        ]
    }
]


const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router


